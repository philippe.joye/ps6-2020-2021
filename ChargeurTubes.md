---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Chargeur de tubes 2
abréviation: MCA-Polytype
filières:
  - Informatique
proposé par étudiant: Lucas Bueche
mandants:
  - Polytype SA
nombre d'étudiants: 1
mots-clés: [Automatique, commande, MCA, Polytyp, IHM]
langue: [F,E,D]
confidentialité: non
réalisation: entreprise
suite: oui
attribué à:
  - Lucas Bueche
nombre d'étudiants: 1
---


## Description/Contexte
Dans le cadre de la "Motion Control Academy" (MCA), l'entreprise Polytype propose de développer dans le cadre d'une équipe interdisciplinaire et sur plusieurs projets d'étudiants un chargeur de tubes automatisé permettant l'automatisation de cette opération. Il s'agit de concevoir, réaliser et mettre en service un chargeur de tubes. Celui-ci devra prendre les tubes, amenés par une table (tapis roulant), et les charger sur les picots d’une chaîne en mouvement.

Le projet de semestre 5 a permis de définir une structure matérielle (mécanique), les interfaces de commande électriques (I/O), ainsi qu'une structure de tâches qui devront s'exécuter en concurrence. Le projet de semestre 6 se propose de poursuivre, pour la partie logicielle, la définition du système. Elle devra notamment explorer les facilités d'interaction humaines (HMI) et le stockage des informations d'exploitation.

## Objectifs/Tâches
L'objectif du projet de semestre est, en collaboration avec les étudiants des filières de Génie mécanique et de Génie électrique, ainsi que l'entreprise Polytype SA, de :

* Structurer et définir, en collaboration avec les étudiants mécanicien et électricien, les API permettant les commandes et les lectures de données nécessaires au poste de commande et de stockage des données de production.

* Concevoir, déployer et valider l'architecture distribuée permettant l'interaction entre l'opérateur et la machine (HMI) afin de fournir les fonctions de réglage et de commande de la machine.

* Concevoir, développer et valider la structure de l'unité d'interaction (HMI) et de stockage des informations de production de la machine.

Plus spécifiquement:

* Proposer une architecture de déploiement logicielle et matérielle N-Tier permettant l'interaction humaine avec la machine et le stockage des données relatives à la production

* Conception des fonctions de commande et configuration de la machine (par exemple, recette)

* Design des API de lecture et d'écriture entre l'automate et le terminal de commande sur la base du protocole OPC-UA

* Définition des fonctions complémentaires permettant l'évolution vers une collecte généralisée des données de production (compatibles industrie 4.0)

## Contraintes
Le projet respectera les contraintes suivantes:

1. L'unité centrale de commande la machine est constituée d'un automate programme de la gamme Siemens fourni par le mandant

2. Le protocole de communication de données entre l'automate (unité de contrôle) et le poste de commande est UPC-UA (Open Platform Communications - Unified Architecture)

## Références
- [OPC-UA fondation web site](https://opcfoundation.org/about/opc-technologies/opc-ua/)
- [OPC-UA online Reference](https://reference.opcfoundation.org/v104/)
- [Free OPC-UA Librariy](https://github.com/FreeOpcUa)
