---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Cryptographie à l'école 2
abréviation: CryptoSchool
filières:
  - Télécommunications
proposé par étudiant: Chloé Amez-Droz
mandants:
  - Haute école pédagogique (HEP)
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Sandy Ingram
mots-clés: [Didactique, HEP, cryptographie]
langue: [F,E,D]
confidentialité: non
réalisation: labo
suite: oui
attribué à:
  - Chloé Amez-Droz
nombre d'étudiants: 1
---
## Description/Contexte
La Haute École pédagogique de Fribourg (HEP|PH FR) forme des enseignants pour les classes primaires et secondaires. Les élèves de classes de l’école obligatoire devront, dans un proche avenir, aborder une nouvelle discipline: les sciences informatiques. L’objectif du projet est de développer, conjointement avec des étudiants de la HEP|PH FR, un environnement numérique qui permet à des élèves du primaire d’acquérir les compétences ciblées. Ainsi, les défis sont certes informatiques, mais aussi didactiques, car l'outil se veut ludique et devra permettre une sensibilisation aux mécanismes de la sécurité des données et é leur confidentialité.

La compétence ciblée : Crypter et décrypter un message à l’aide d'une méthode simple (p. ex.: César)

Le projet de semestre 5 a permis de définir les règles de base d'une application multi-utilisateurs basées sur le jeu du "loup Garou". L'architecture globale a également été définie et un premier prototype implémentant un "chat" a été réalisé. Le projet de semestre 6 se propose de poursuivre, le développement du système sur les bases validées du prototype. Elle devra notamment finaliser les scénarii de jeux et cas d'utilisation établis précédemment afin d'aboutir sur un prototype opérationnel.

## Contraintes

* Collaborer avec des étudiants de la HEP|PH FR pour assurer la qualité didactique des scénarios et des activités proposées à travers le jeu.

* Développer un environnement numérique qui permettra, dans les années à venir, d’accueillir d’autres activités ciblant d’autres compétences.

* Approche et développement du projet utilisant une approche "Open Source"

* Mettre en production le développement sur les solutions d’hébergement de la HEP|PH FR, afin de pouvoir utiliser la solution développée dans les classes primaires/secondaires du canton de Fribourg.

## Objectifs/Tâches
* finalisation des scénarios du jeu du "loup Garou" en collaboration avec les étudiants de la HEP

* finaliser l'architecture logicielle de la solution proposée dans le cadre du projet de semestre 5

* concevoir, développer et valider les fonctions de chat et le cryptage des messages

* valider en situation réelle les outils développés en jouant les scénarios principaux
