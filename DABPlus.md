---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Software Defined Radio on DAB+
abréviation: DAB+
filières:
  - Télécommunications
mandants:
  - Laboratoire Télécom
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Jean-Frédéric Wagen
mots-clés: [Software Defined Radio,modulation, USRP]
langue: [F,E,D]
confidentialité: non
réalisation: labo
suite: non
nombre d'étudiants: 1
---
## Description/Contexte
DAB+ est l'abréviation anglaise de "Digital Audio Broadcasting". Plus simplement, il s'agit du nom de la radio numérique terrestre (RNT), une technologie complémentaire à la bande FM.

Chaque signal de service est codé individuellement au niveau de la source, protégé contre les erreurs et entrelacé dans le temps dans le codeur de canal. Les services sont ensuite multiplexés dans le Main Service Channel (MSC), selon un multiplexage prédéfini, mais ajustable. La sortie du multiplexeur est ensuite mixé avec les informations de contrôle et de service du multiplex, pour former les trames de transmission. Enfin, le multiplexage par répartition orthogonale de la fréquence (OFDM) est appliqué pour former le signal DAB, qui est constitué d'un grand nombre de porteuses. Le signal est ensuite transposé dans la bande de fréquence radio appropriée, amplifié et transmis.

L'objectif du projet est d'implémenter la réception et la démodulation de programme de radio DAB+ à l'aide des équipement USRP de National Instruments (développement à l’aide de LabVIEW "standard" ou LabVIEW Communications) à disposition dans nos laboratoires du C10.

## Contraintes

* Utilisation des équipements USRP de National Instruments et son système de modélisation

## Objectifs/Tâches

* Décrire de manière précise les modes et les techniques de modulation du DAB

* Explorer et évaluer les solutions "low cost" du marché

* Prise en main des équipement USRP et de son environnement LabVIEW

* Assurer la réception du signal DAB de manière à pouvoir le décoder proprement

* Structurer et modéliser le processus décodage du DAB

* Concevoir, développer et valider les fonctions de démodulations DAB

## Références
- [How works DAB](https://www.worlddab.org/dab/how-dab-works)
- [15 Best Software Defined Radios (SDRs) For Any Budget](https://fromdc2daylight.com/10-best-software-defined-radios-sdrs/)
- [GNU Radio](https://www.gnuradio.org/)

